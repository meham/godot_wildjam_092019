## What?!?

A big trembling abstract creature in nothingness sends you out to find everything you can
and bring it back, so it can **merge** your findings and you back into itself
and grow.

Then the creature produces a new you.

That's it.

---

## But why??

Game Jam entry for [Godot Wild Jam #13](https://itch.io/jam/godot-wild-jam-13)

**Theme:** Merge

---

## How?

 - [Godot Engine 3.1](https://godotengine.org)
 - [Sublime Text](https://www.sublimetext.com)
 - [Git Extensions](https://gitextensions.github.io)
 - [Blender](https://blender.org)

## Credits
 - [Ryoichi Tsunekawa for Bebas font](https://github.com/dharmatype/Bebas)