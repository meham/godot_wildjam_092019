extends RigidBody

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var speed = 1.0;
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_pressed("game_forward"):
		apply_impulse(Vector3(0,0,0)*speed, Vector3(0,0,1)*speed)
	if Input.is_action_pressed("game_backward"):
		apply_impulse(Vector3(0,0,0)*speed, Vector3(0,0,-1)*speed)

